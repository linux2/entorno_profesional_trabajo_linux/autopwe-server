Referencias
===========

 - https://linoxide.com/how-tos/install-tmux-manage-multiple-linux-terminals/
 - https://hackernoon.com/a-gentle-introduction-to-tmux-8d784c404340


Help
====
Just type "Ctrl-a and '?'" to see a list of available commands.

Configuring Tmux
================

- Tmux is highly configurable. You can edit "tmux.conf" file to do this. If you donât have the file, you can create it. 
- For system wide, you can put the tmux.conf in /etc folder. Or put it on ~/.tmux.conf for user specific settings. 
- A continuaciÃ³n una serie de cambios:

a) Change the "Prefix Key":

unbind C-b
set -g prefix C-a

b) Change the splitting panes:

unbind %
bind h split-window -v
unbind '"'
bind v split-window -h

- % sign into h letter for vertically split
- â sign into v letter for horizontally split

c) Change the status bar bar looks:

# Status bar theme
set -g status-bg black
set -g status-fg white

# Highlight and Notify
set-window-option -g window-status-current-bg red
setw -g monitor-activity on
set -g visual-activity on


Window (Tabs)
=============
 - New window: Ctrl-a and 'c'
 - Name/Rename window: Ctrl-a and ','
 - List windows: Ctrl-a and 'w'
 - Close a window, simply press (Ctrl-b) + &
 - Switch between windows: 
      - Previous: Ctrl-a and 'p'
      - Next: Ctrl-a and 'n'
      - Go to specific (by its number) window: Ctrl-a and '<number>'

Session Management
==================
 - :new<CR> new session
 - List sessions: Ctrl-a and 's'
 - Name session: Ctrl-a and '$'
 - Detach your session: Ctrl-a and 'd'
 - Re-attach: 
              1) tmux ls
              2) tmux attach -t <number_from_list>

Panes
=====
 - Split Window horizontally: Ctrl-a and 'h'
 - Split Window vertically: Ctrl-a and 'v'
 - Number of panes: Ctrl-a and 'q'
 - Move Left : (Ctrl-a) + Left arrow OR (Ctrl-a) + {
 - Move Right : (Ctrl-a) + Right arrow OR (Ctrl-a) + }
 - Move Up : (Ctrl-a) + + Up arrow
 - Move Down : (Ctrl-a) + Down arrow
 - Move to the next pane : (Ctrl-a) + o
 - Show number for each panes and press the number : (Ctrl-a) + b q + pane number. For example : (Ctrl-a) + b q + 1 will move you to pane number 1
 - Pane Full Screen: Ctrl-a and 'z' (Hit Ctrl-a and 'z' again to shrink it)
 - Resize pane in direction of <arrow key>: Ctrl-a and Ctrl-<arrow key>

