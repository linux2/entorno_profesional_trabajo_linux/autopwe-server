#!/bin/bash

# Colours
green="\e[0;32m\033[1m"
end="\033[0m\e[0m"
red="\e[0;31m\033[1m"
blue="\e[0;34m\033[1m"
yellow="\e[0;33m\033[1m"
purple="\e[0;35m\033[1m"
cyan="\e[0;36m\033[1m"
gray="\e[0;37m\033[1m"

# User paths
userPath=/home/esb
rootPath=/root
cwd=$(pwd)

# Banner
function banner() {
    # https://patorjk.com/software/taag/#p=display&h=0&f=ANSI%20Shadow&t=Auto-PWE-Server
    echo -e "


 █████╗ ██╗   ██╗████████╗ ██████╗       ██████╗ ██╗    ██╗███████╗      ███████╗███████╗██████╗ ██╗   ██╗███████╗██████╗      ${cyan}Auto Professional Working Environmen Server Edition (Created by ${blue}ESB${end}${cyan})${end}
██╔══██╗██║   ██║╚══██╔══╝██╔═══██╗      ██╔══██╗██║    ██║██╔════╝      ██╔════╝██╔════╝██╔══██╗██║   ██║██╔════╝██╔══██╗
███████║██║   ██║   ██║   ██║   ██║█████╗██████╔╝██║ █╗ ██║█████╗  █████╗███████╗█████╗  ██████╔╝██║   ██║█████╗  ██████╔╝     ${yellow}[*]${end} Script to automatically install your customized Parrot working environment.
██╔══██║██║   ██║   ██║   ██║   ██║╚════╝██╔═══╝ ██║███╗██║██╔══╝  ╚════╝╚════██║██╔══╝  ██╔══██╗╚██╗ ██╔╝██╔══╝  ██╔══██╗     ${yellow}[*]${end} Inspired by ${red}S4vitar${end}
██║  ██║╚██████╔╝   ██║   ╚██████╔╝      ██║     ╚███╔███╔╝███████╗      ███████║███████╗██║  ██║ ╚████╔╝ ███████╗██║  ██║     ${yellow}[*]${end} Video by S4vitar: https://www.youtube.com/watch?v=mHLwfI1nHHY
╚═╝  ╚═╝ ╚═════╝    ╚═╝    ╚═════╝       ╚═╝      ╚══╝╚══╝ ╚══════╝      ╚══════╝╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚══════╝╚═╝  ╚═╝

"
}



#############
# FUNCTIONS #############################################################################################################
#############


# System Update & Upgrade
function sys_update(){
    apt-get update && apt-get upgrade -y
}

# Install all the required packages
function reqs(){

    # General purpose packages:
    apt install build-essential git vim unzip xcb libxcb-util0-dev libxcb-ewmh-dev libxcb-randr0-dev libxcb-icccm4-dev libxcb-keysyms1-dev libxcb-xinerama0-dev libasound2-dev libxcb-xtest0-dev libxcb-shape0-dev -y
}


# HackNerd fonts installation
function hacknerdfonts_install(){
    cd $cwd
    wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.2.2/Hack.zip
    unzip Hack.zip
    cp *.ttf /usr/local/share/fonts/
    fc-cache -v
}

# Nano configuration
function nano_config(){
    cp $cwd/nanorc $userPath/.nanorc
}

#Powerlevel10K and ZSH related installation and configuration
function p10k_zsh_installation(){

    # P10K for "esb" user:
    git clone --depth=1 https://github.com/romkatv/powerlevel10k.git $userPath/powerlevel10k
    cp $cwd/zshrc $userPath/.zshrc
    cp $cwd/p10k.zsh $userPath/.p10k.zsh

    # P10K for "root" user:
    git clone --depth=1 https://github.com/romkatv/powerlevel10k.git $rootPath/powerlevel10k
    cp $cwd/p10k.zsh.root $rootPath/.p10k.zsh

    # Symbolic link of "zshrc" file for "root" user:
    ln -s -f $userPath/.zshrc $rootPath/.zshrc

    # Set default shell to "zsh" for "esb" and "root" users:
    usermod --shell /usr/bin/zsh esb
    usermod --shell /usr/bin/zsh root

    # Minor fix about problems with permissions when migrating to a user from "root" through "su" command:
    chown esb:esb /root
    chown esb:esb /root/.cache -R
    chown esb:esb /root/.local -R
}

# {zsh-syntax-highlighting zsh-autocomplete zsh-autosuggestions} plugins installation
function zsh_plugins_installation(){
    cd $cwd
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git
    git clone https://github.com/marlonrichert/zsh-autocomplete.git
    git clone https://github.com/zsh-users/zsh-autosuggestions

    sudo mv zsh-syntax-highlighting /usr/share
    sudo mv zsh-autocomplete /usr/share
    sudo mv zsh-autosuggestions /usr/share
}

# BAT installation
function bat_install(){
    cd $cwd
    wget https://github.com/sharkdp/bat/releases/download/v0.22.1/bat_0.22.1_amd64.deb
    sudo dpkg -i bat_0.22.1_amd64.deb
}

# LSD installation
function lsd_install(){
    cd $cwd
    wget https://github.com/Peltoche/lsd/releases/download/0.23.0/lsd_0.23.0_amd64.deb
    sudo dpkg -i lsd_0.23.0_amd64.deb
}

# EXA installation
function exa_install(){
    cd $cwd
    wget https://github.com/ogham/exa/releases/download/v0.10.1/exa-linux-x86_64-v0.10.1.zip
    unzip exa-linux-x86_64-v*.zip -d exa
    sudo mv exa/bin/exa /usr/local/bin/
}

# FZF installation
function fzf_install(){

    # For "esb" user:
    git clone --depth 1 https://github.com/junegunn/fzf.git $userPath/.fzf
    chown -R esb:esb $userPath/.fzf
    su esb -c "$userPath/.fzf/install"

    # For "root" user:
    git clone --depth 1 https://github.com/junegunn/fzf.git $rootPath/.fzf
    $rootPath/.fzf/install
}

# Oh-My-Tmux installation
function omt_install(){
    # For "esb" user:
    cd $userPath
    git clone https://github.com/gpakosz/.tmux.git
    ln -s -f .tmux/.tmux.conf
    cp .tmux/.tmux.conf.local .

    # For "root" user:
    git clone https://github.com/gpakosz/.tmux.git $rootPath/.tmux
    ln -s -f $rootPath/.tmux/.tmux.conf $rootPath/.tmux.conf
    cp $rootPath/.tmux/.tmux.conf.local $rootPath/
}

# "WhichSystem.py" program installation
function whichsystem_install(){
    cd $cwd
    cp scripts/whichSystem.py /usr/bin/whichSystem.py
    chmod +x /usr/bin/whichSystem.py
}

# "FastTCPScan.go" program installation
function fasttcpscan_install(){
    cd $cwd
    cp scripts/fastTCPScan /usr/bin/fastTCPScan
    chmod +x /usr/bin/fastTCPScan
}

# Copy HowTos folder
function howtos(){
    cd $cwd
    cp -R 0-HowTos $userPath
}

# Set ownership of "esb" user folders and files
function ownership_config(){
    cd $userPath
    chown -R esb:esb .tmux/ .tmux.conf.local .tmux.conf .p10k.zsh .nanorc wallpapers/ powerlevel10k/ 0-HowTos/
}

# htbExplorer install
function htbExplorer_install(){
    cd $userPath
    git clone https://github.com/s4vitar/htbExplorer.git
    chown -R esb:esb /home/esb/htbExplorer
    ln -s /home/esb/htbExplorer/htbExplorer /usr/bin
}


########
# MAIN #############################################################################################################
########

## git clone https://github.com/jmlgomez73/Shockz-MKE && cd Shockz-MKE && chmod +x install.sh && ./install.sh

# Check user privileges
[ "$(id -u)" -ne 0 ] && (echo -e "${red} [!] This script must be run as root ${end}" >&2) && exit 1;

# Show banner
banner

# System update & upgrade
sys_update

# Install all the required packages
reqs

# HackNerd Fonts installation
hacknerdfonts_install

# Nano configuration
nano_config

# Powerlevel10K and ZSH related configuration
p10k_zsh_installation

# {zsh-syntax-highlighting zsh-autocomplete zsh-autosuggestions} plugins installation
zsh_plugins_installation

# BAT installation
bat_install

# LSD installation
lsd_install

# EXA installation
exa_install 

# FZF installation
fzf_install

# Oh-my-tmux installation
omt_install

# "WhichSystem.py" program installation
whichsystem_install

# "FastTCPScan.go" program installation
fasttcpscan_install

# Copy HowTos folder
howtos

# Set ownershipt of "esb" folders and files
ownership_config

# htbExplorer
htbExplorer_install

# Installation Finished.Post-Installation steps:
echo -e "\n${yellow}[*]${end} Installation finished!"
echo -e "${yellow}[*]${end} After system reboot, please consider performing the following steps: "
echo -e "\t${yellow}1)${end} Terminal related configuration (HackNerd fonts, customization, etc.)"
echo -e "\t${yellow}2)${end} \"FoxyProxy\" Firefox plugin installation."
echo -e "\t${yellow}3)${end} \"htbExplorer\" API configuration."
