# Colours
greenColour="\e[0;32m\033[1m"
endColour="\033[0m\e[0m"
redColour="\e[0;31m\033[1m"
blueColour="\e[0;34m\033[1m"
yellowColour="\e[0;33m\033[1m"
purpleColour="\e[0;35m\033[1m"
turquoiseColour="\e[0;36m\033[1m"
grayColour="\e[0;37m\033[1m"

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Prompt
PROMPT="%F{red}┌[%f%F{cyan}%m%f%F{red}]─[%f%F{yellow}%D{%H:%M-%d/%m}%f%F{red}]─[%f%F{magenta}%d%f%F{red}]%f"$'\n'"%F{red}└╼%f%F{green}$USER%f%F{yellow}$%f"
# Export PATH$
export PATH=~/.local/bin:/snap/bin:/usr/sandbox/:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/usr/share/games:/usr/local/sbin:/usr/sbin:/sbin:$PATH

# Fix the Java Problem
export _JAVA_AWK_WM_NONREPARENTING=1

#####################################################
# Auto completion / suggestion
# Mixing zsh-autocomplete and zsh-autosuggestions
# Requires: zsh-autocomplete (custom packaging by Parrot Team)
# Jobs: suggest files / foldername / histsory bellow the prompt
# Requires: zsh-autosuggestions (packaging by Debian Team)
# Jobs: Fish-like suggestion for command history
source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh-autocomplete/zsh-autocomplete.plugin.zsh
# Select all suggestion instead of top on result only
zstyle ':autocomplete:tab:*' insert-unambiguous yes
zstyle ':autocomplete:tab:*' widget-style menu-select
zstyle ':autocomplete:*' min-input 2
bindkey $key[Up] up-line-or-history
bindkey $key[Down] down-line-or-history
bindkey  "^[[H"   beginning-of-line
bindkey  "^[[F"   end-of-line
bindkey  "^[[3~"  delete-char

##################################################
# Fish like syntax highlighting
# Requires "zsh-syntax-highlighting" from apt

source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Save type history for completion and easier life
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt histignorealldups sharehistory
#setopt appendhistory

# Useful alias for benchmarking programs
# require install package "time" sudo apt install time
# alias time="/usr/bin/time -f '\t%E real,\t%U user,\t%S sys,\t%K amem,\t%M mmem'"
# Display last command interminal
echo -en "\e]2;Parrot Terminal\a"
preexec () { print -Pn "\e]0;$1 - Parrot Terminal\a" }

source ~/powerlevel10k/powerlevel10k.zsh-theme

#########
# ALIAS ########################################################################
#########

#alias ls='ls -lh --color=auto'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# Custom ESB Aliases

alias ..='cd ..'
alias c='clear'
alias cat='bat'
alias catn='/bin/cat'
alias catnl='/bin/bat --paging=never'
alias esb_shortcuts='cat ~/0-HowTos/0-Atajos_de_teclado.txt'
alias ll='lsd -lh --group-dirs=first'
alias la='lsd -a --group-dirs=first'
alias l='lsd --group-dirs=first'
alias ls='lsd -lh --group-dirs=first'
alias misfunciones='more +120 ~/.zshrc'
alias misalias='more +64 ~/.zshrc'
alias poweroff='sudo poweroff'
alias reload='source ~/.zshrc'
alias reboot='sudo reboot'
alias tty='~/.config/bin/tty.sh'
alias update='sudo apt-get update'
alias upgrade='sudo apt-get upgrade'


# Alias HowTos:
# ------------

alias mishowtos='more ~/0-HowTos/00-Mis_howtos_terminal.txt'

alias howtobashscripting='c && more ~/0-HowTos/scripting/1-Bash_scripting.txt'
alias howtocurl='c && more ~/0-HowTos/linux_general/05-curl.txt'
alias howtocurl='c && more ~/0-HowTos/linux_general/05-curl.txt'
alias howtodockerbasico='c && more ~/0-HowTos/docker/01-comandos_basicos.txt'
alias howtodockertipico='c && more ~/0-HowTos/docker/02-comandos_tipicos.txt'
alias howtofind='c && more ~/0-HowTos/linux_general/06-Busquedas.txt'
alias howtogit='c && more ~/0-HowTos/git_github_gitlab/01-pasos_resumidos_git_github.txt'
alias howtogpg='c && more ~/0-HowTos/gpg/01-GPG_comandos_generales.txt'
alias howtogrepegrep='c && more ~/0-HowTos/linux_general/03-grep_egrep.txt'
alias howtoiptables='c && more ~/0-HowTos/networking/01-comandos_generales_iptables.txt'
alias howtolinuxusergroups='c && more ~/0-HowTos/linux_general/01-Gestion_usuarios_grupos_Linux.txt'
alias howtomongodb='c && more ~/0-HowTos/databases/mongodb/01-comandos_utiles_mongodb.txt'
alias howtomountusb='c && more ~/0-HowTos/linux_general/07-montar_usb_drives_howto.txt'
alias howtomysql='c && more ~/0-HowTos/databases/mysql/01-comandos_utiles_mysql.txt'
alias howtonetcat='c && more ~/0-HowTos/networking/06-netcat.txt'
alias howtonetstat='c && more ~/0-HowTos/networking/04-Netstat.txt'
alias howtoneovim='c && more ~/0-HowTos/linux_general/10-neovim_nvim.txt'
alias howtonmap='c && more ~/0-HowTos/networking/03-NMAP.txt'
alias howtopip='c && more ~/0-HowTos/python/04-Pip_howto.txt'
alias howtopostgresql='c && more ~/0-HowTos/databases/postgresql/01-comandos_utiles_postgresql.txt'
alias howtopythonproject='c && more ~/0-HowTos/python/01-python_start_project.txt'
alias howtoregex='c && more ~/0-HowTos/regex/01-Resumen_RegEx.txt'
alias howtorouting='c && more ~/0-HowTos/networking/02-linux_routing.txt'
alias howtorsyncbackup='c && more ~/0-HowTos/rsync/01-Automatizar_backups_con_RSYNC.txt'
alias howtosed='c && more ~/0-HowTos/linux_general/02-sed.txt'
alias howtoservices='c && more ~/0-HowTos/linux_general/08-Services_updaterc.txt'
alias howtossh='c && more ~/0-HowTos/ssh/01-SSH_y_GitHub.txt'
alias howtosshtunneling='c && more ~/0-HowTos/ssh/02-SSH_Tunneling.txt'
alias howtotargzip='c && more ~/0-HowTos/linux_general/04-Comprimiendo_con_tar_gzip_linea_de_comandos.txt'
alias howtotcpdump='c && more ~/0-HowTos/networking/05-tcpdump.txt'
alias howtotmux='c && more ~/0-HowTos/tmux/01-tmux_howto.txt'
alias howtovim='c && more ~/0-HowTos/vim_neovim/01-vim_comandos_basicos.txt'
alias howtovirtualenv='c && more ~/0-HowTos/python/02-VirtualEnvironments_Venv_Pipenv.txt'
alias howtovirtualenvwrapper='c && more ~/0-HowTos/python/03-howtovirtualenvwrapper.txt'
alias howtowificli='c && more ~/0-HowTos/linux_general/09-wifi_cli_archlinux.txt'


# Alias Pentesting:
alias howto_preparativos_tmux='cat ~/0-HowTos/pentesting/0-Preparativos_TMUX.txt'
alias howto_osint='cat ~/0-HowTos/pentesting/1-OSINT.txt'
alias howto_recon-enum='cat ~/0-HowTos/pentesting/2-Reconocimiento_Enumeration.txt'
alias howto_vuln-scan='cat ~/0-HowTos/pentesting/3-Vulnerability_Scan.txt'
alias howto_web-attacks='cat ~/0-HowTos/pentesting/4-Web_Attacks.txt'
alias howto_network-attacks='cat ~/0-HowTos/pentesting/5-Network_Attacks.txt'
alias howto_reverse-shells='cat ~/0-HowTos/pentesting/6-Reverse_Shells.txt'
alias howto_searchsploit='cat ~/0-HowTos/pentesting/7-Searchsploit.txt'
alias howto_tratamiento_TTY='cat ~/0-HowTos/pentesting/8-Tratamiento_TTY.txt'
alias howto_wifi_pentesting='cat ~/0-HowTos/pentesting/9-Wi-Fi_Pentesting.txt'
alias howto_burpsuite='cat ~/0-HowTos/pentesting/A-Burpsuite.txt'


# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ $KEYMAP == vicmd ]] || [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ $KEYMAP == main ]] || [[ $KEYMAP == viins ]] || [[ $KEYMAP = '' ]] || [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select

# Start with beam shape cursor on zsh startup and after every command.
zle-line-init() { zle-keymap-select 'beam'}

########################
# CUSTOM ESB FUNCTIONS ##################################################
########################

# Lista de howtos relativos al pentesting
function esb_hacking_howtos(){

	echo -e "\n 0) ESB Shortcuts: \t\t\t >esb_shortcuts"
    echo -e " 1) Preparativos TMUX: \t\t\t >howto_preparativos_tmux"
    echo -e " 2) OSINT: \t\t\t\t >howto_osint"
    echo -e " 3) Reconnaissance - Enumeration: \t >howto_recon-enum"
    echo -e " 4) Vulnerability Scan: \t\t >howto_vuln-scan"
    echo -e " 5) Web Attacks: \t\t\t >howto_web-attacks"
    echo -e " 6) Network Attacks: \t\t\t >howto_network-attacks"
    echo -e " 7) Reverse Shells: \t\t\t >howto_reverse-shells"
    echo -e " 8) Searchsploit: \t\t\t >howto_searchsploit"
    echo -e " 9) Tratamiento TTY: \t\t\t >howto_tratamiento_tty"
    echo -e " 10) Wi-Fi Pentesting: \t\t\t >howto_wifi_pentesting\n"
    echo -e " =================================================================== \n"
    echo -e " A) Burpsuite: \t\t\t\t >howto_burpsuite\n"
}

# Make tree work directory structure
function mkt(){
    mkdir {nmap,content,scripts,tmp,exploits}
}

# Extract port from "allPorts" file from nmap
function extractPorts(){
    echo -e "\n${yellowColour}[*] Extracting information...${endColour}\n"

    ip_address=$(cat allPorts | grep -oP '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}' | sort -u)
    open_ports=$(cat allPorts | grep -oP '\d{1,5}/open' | awk '{print $1}' FS="/"| xargs | tr ' ' ',')

    echo -e "\t${blueColour}[*] IP Address: ${endColour}${grayColour}$ip_address${endColour}"
    echo -e "\t${blueColour}[*] Open Ports: ${endColour}${grayColour}$open_ports${endColour}\n"

    echo $open_ports | tr -d "\n" | xclip -sel clip

    echo  -e "${yellowColour}[*] Ports have been copied to clipboard!${endColour}\n"
}

# Extract IP from "allIP" file from "hostDiscovery.sh"
function extractIPs(){
    echo -e "\n${yellowColour}[*] Extracting IP addresses...${endColour}\n"

    ip_address=$(cat allIP | grep -oP '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}' | xargs)

    echo -e "\t${blueColour}[*] IP Address: ${endColour}${grayColour}$ip_address${endColour}"

    echo $ip_address | tr -d "\n" | xclip -sel clip

    echo  -e "${yellowColour}[*] IPs have been copied to clipboard!${endColour}\n"
}

# Eliminación profunda de archivos
function rmk(){
	scrub -p dod $1
	shred -zun 10 -v $1
}

# Definir el "target" para el bloque de la barra superior
function settarget(){
    target_ip=$1
    target_name=$2
    echo "$target_ip $target_name" > ~/.config/bin/target
}

# Eliminar/varcias el valor del bloque "target" de la barra superior
function cleartarget(){
    echo '' > ~/.config/bin/target
}

# Golang vars
export GOROOT=/usr/local/go
export GOPATH=$HOME/go
export PATH=$GOPATH/bin:$GOROOT/bin:$HOME/.local/bin:$PATH
